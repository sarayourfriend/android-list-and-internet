package com.example.sara.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity {

    private TextView textView;

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.viewText);
        gridView = findViewById(R.id.gridview);

    }

    public void onClick(View view) {
        textView.setText("loading!");

        RequestQueue queue = Volley.newRequestQueue(this);

        // each time we call get... it creates a future and starts the request
        // the future represents when the request is finished (after it has its
        // .complete method called

        final CompletableFuture<String> duckDuckGo = getDuckDuckGo(queue);
        final CompletableFuture<String> google = getGoogle(queue);

        List<CompletableFuture<String>> futures = new ArrayList<>();
        futures.add(duckDuckGo);
        futures.add(google);

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).thenApply(new Function<Void, Void>() {
            public Void apply(Void o) {
                // now we know both the futures are completed
                // here we can safely call .get on the future!

                try {
                    String googleResponse = google.get();
                    String duckDuckGoResponse = duckDuckGo.get();

                    String toPut = "google: " + googleResponse.substring(0, 25) + "\nduckDuckGo: " + duckDuckGoResponse.substring(0, 25);
                    textView.setText(toPut);
                    // the | down here is a way to catch multiple types of errors in the same catch block
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        
//        gridView.setAdapter(new FooAdapter(this, new ArrayList<String>() {{ add("Hello!"); add("World!"); }}));
    }

    public CompletableFuture<String> getDuckDuckGo(final RequestQueue queue) {
        String url = "https://www.duckduckgo.com";

        final CompletableFuture<String> f = new CompletableFuture<>();

        // https://developer.android.com/training/volley/requestqueue#singleton
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                f.complete(response.substring(0, 500));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                f.complete(error.getMessage());
            }
        });
        queue.add(strReq);
        return f;
    }

    public CompletableFuture<String> getGoogle(final RequestQueue queue) {
        String url = "https://www.google.com";

        final CompletableFuture<String> f = new CompletableFuture<>();
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                f.complete(response.substring(0, 500));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                f.complete(error.getMessage());
            }
        });
        queue.add(strReq);
        return f;
        // https://developer.android.com/training/volley/requestqueue#singleton
    }
}
