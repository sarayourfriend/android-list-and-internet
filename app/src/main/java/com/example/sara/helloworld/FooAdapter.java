package com.example.sara.helloworld;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class FooAdapter extends BaseAdapter {

    private Context context;
    private List<String> items;

    public FooAdapter(Context context, List<String> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        if (items == null) return null;
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        String content = (String) getItem(position);
        if (convertView == null) {
            textView = new TextView(context);
            textView.setText(content);
            textView.setWidth(20);
            textView.setHeight(100);
        } else {
            textView = (TextView) convertView;
        }

        textView.setText(content);

        return textView;
    }
}
